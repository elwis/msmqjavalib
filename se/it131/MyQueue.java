package se.it131;
import java.io.*;
import ionic.Msmq.*;

import java.util.List;
import java.util.Date;
import java.text.SimpleDateFormat;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;

public class MyQueue {

	private String queuein;
	private String queueout;
	private String folderIn;
	private String folderOut;
	private Message msg;
	
	public MyQueue(String queuein, String queueout, String folderIn, String folderOut) {
		super();
		this.queuein = queuein;
		this.queueout = queueout;
		this.folderIn = folderIn;
		this.folderOut = folderOut;
	}

	public boolean readFromQueue() throws MessageQueueException
	{
		Queue queue= new Queue(queuein);
		boolean receiving = true;
		
		while (receiving == true)
		{
			Message msg = getMessageFromQueue(queue);
			if(msg == null)
				receiving = false;
			else
				try {
					writeToFile(msg);
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		
		return true;
	}
	
	private Message getMessageFromQueue(Queue queue) {
		try {
		     
            msg = queue.receive(4000);
            return msg;

		 }
		 catch (MessageQueueException ex1) {
		     System.out.println("Read failure: " + ex1.toString());
		     return null;
		 }
	}
	
	private boolean sendMessageToQueue(Queue queue, Message m) {
		try {
		     
		     byte[] correlationId = { 0,2,4,6,8,9 }; 
		     m.setCorrelationId(correlationId);
		     queue.send(m);
            return true;

		 }
		 catch (MessageQueueException ex1) {
		     System.out.println("Read failure: " + ex1.toString());
		     return false;
		 }
	}
	public boolean writeToQueue(Message m) throws MessageQueueException
	{
		//writing messages
		Queue queue= new Queue(queueout);
		boolean ok = sendMessageToQueue(queue, m);
		Close(queue);
		if(ok)
			return true;
		else
			return false;
	}
	
	public boolean readFromFile()
	{
		File dir = new File(folderIn);
		String[] extensions = new String[] { "txt", "jsp" };
		try {
			System.out.println("Getting files in " + dir.getCanonicalPath() + " not including subdirectories");
			List<File> files = (List<File>) FileUtils.listFiles(dir, extensions, false);
			for (File file : files) {
				System.out.println("file: " + file.getCanonicalPath());
				msg = new Message(FileUtils.readFileToString(file, "UTF-8"));
				msg.setLabel(new SimpleDateFormat("MM/dd/yyyy-HH:mm:ss").format(new Date()));
				writeToQueue(msg);
				
			}
		}catch (IOException | MessageQueueException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean writeToFile(Message msg) throws UnsupportedEncodingException
	{
		File pathAndname = null;
	    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd_HHmmss");//dd/MM/yyyy
	    Date now = new Date();
	    String strDate = sdfDate.format(now);
		pathAndname = new File(folderOut +"\\"+strDate+".txt");
		System.out.println("pathAndname: " + pathAndname);
		try {
			FileUtils.writeStringToFile(pathAndname, msg.getBodyAsString(), "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	 private void Close(Queue queue) {
	     try {
	         checkOpen(queue);
	         System.out.println("close");
	         queue.close();
	         queue= null;
	         System.out.println("close: OK.");
	     }
	     catch (MessageQueueException ex1) {
	         System.out.println("close failure: " + ex1);
	     }
	 }
	 
	 private void checkOpen(Queue queue)
		     throws MessageQueueException {
		     if (queue==null)
		         throw new MessageQueueException("Queue doesn't exist\n",-1);
		 }

	
	public void run()
	{
		
		System.out.println("Starting Connector..");
		System.out.println(queuein +"," +queueout+"," + folderIn +","+folderOut);
		boolean okFromFile = readFromFile();
		if(okFromFile)
			System.out.println("All is well in Dalum");
		try {
			boolean okFromQueue = readFromQueue();
			System.out.println("All is well in birmingham");
		} catch (MessageQueueException e) {
			e.printStackTrace();
			System.out.println("Something crashed and burned");
		}
	    

	}

}
