package se.it131;
import java.io.File;

public class FilenameFilter {
	    public boolean accept(File dir, String name) {
	        return name.endsWith(".txt");
	    }
	};

